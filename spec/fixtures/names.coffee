fs          = require 'fs'
{namesfile} = require '../../config'
namesfile   = "#{process.cwd()}/#{namesfile}"

try
  namesThatEcho = fs.readFileSync(namesfile, 'utf-8').split(',')
catch err
  console.error "Error reading namesfile at #{namesfile}:", err

module.exports = namesThatEcho
