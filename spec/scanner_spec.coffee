Scanner  = require '../src/scanner'
comments = require './fixtures/comments'
names    = require './fixtures/names'

# Create a mock store with only features used by scanner
class MockStore
  constructor: (@items) ->
  dequeue: -> @items.shift()
  peek:    -> @items[0]
  empty:   -> @items.length is 0

describe 'Scanner', ->
  describe '#scanStore', ->
    it 'should return an array echoey comments', ->
      store   = new MockStore comments
      scanner = new Scanner store, names

      echoes  = scanner.scanStore()
      expectedEchoes = [
        { parent: "c", subreddit: "t", text: "((((BLUMENSTEIN))))" }
        { parent: "f", subreddit: "t", text: "((((LEIBOWITZ))))" }
        { parent: "i", subreddit: "t", text: "((((BLUM))))\n((((HIRSCH))))" }
        { parent: "k", subreddit: "t", text: "((((FRIDBERG))))" }
      ]

      expect(echoes).to.deep.eq(expectedEchoes)
