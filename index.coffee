fs          = require 'fs'
{argv}      = require 'yargs'
echobot     = require './src'
{namesfile} = require './config'
debug       = require('debug') 'echobot'

# Create array of echoey names from namesfile
try
  namesThatEcho = fs.readFileSync(namesfile, 'utf-8').split(',')
catch err
  debug "Error reading namesfile at #{namesfile}"
  debug err
  return

# Start (((echoing)))
echobot namesThatEcho, argv
