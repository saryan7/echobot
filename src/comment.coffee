Thing   = require './thing'
{depth} = require '../config'

# A leddit comment
class Comment extends Thing
  # Fetch comments from leddit post
  @fetch: (store, reddit, post, cb) ->
    reddit.comments {
      link:  post.id
      depth: depth
      limit: 500
    }, cb

module.exports = Comment
