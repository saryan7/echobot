async  = require 'async'
plural = require 'pluralize'
debug  = require('debug') 'echobot'

class Commenter
  constructor: (@comments, @bot) ->
    @timeMatcher = /(\d+)\s(second|minute|hour)/i
    @aMinute     = 60 * 1000

  nameTheJews: (defer) ->
    async.eachSeries @comments, ((comment, cb) =>
      @_submitComment comment, cb
    ), -> defer.resolve()
  
  _submitComment: (comment, cb) ->
    debug "Naming the jew..."
    setTimeout ( =>
      @bot.reddit.comment comment.parent, comment.text, (err) =>
        if err
          if @_atRateLimit err
            @_handleRateLimitError err, comment, cb
          else if @_areBanned err
            @_handleBannedError comment, cb
          else
            @_handleUnknownError err, cb
        else
          @_handleSuccess comment, cb
    ), @bot.timer.nextRequestIn()

  _handleSuccess: (comment, cb) ->
    debug "Revealing comment submitted to /r/#{comment.subreddit}"
    @bot.timer.logRequest()
    cb()

  _atRateLimit: (err) ->
    typeof err is 'object' and err[0] is 'RATELIMIT'

  _handleRateLimitError: (err, comment, cb) ->
    waitTime = @_parseWaitTime err
    debug "At comment rate limit. Waiting #{@_toMinutes waitTime}..."
    submitCountdown = @_countdown waitTime
    setTimeout ( =>
      clearInterval submitCountdown
      @_submitComment comment, cb
    ), waitTime

  _parseWaitTime: (err) ->
    message = err[1]
    [m, time, unit] = message.match @timeMatcher
    # Using plain JewS here because coffeescript includes breaks in switches
    `switch (unit) {
      case 'hour':   time *= 60;
      case 'minute': time *= 60;
      case 'second': time *= 1000;
    }`
    time + @aMinute

  _countdown: (time) ->
    setInterval ( =>
      time -= @aMinute
      debug "Waiting #{@_toMinutes time}..."
    ), @aMinute

  _toMinutes: (ms) ->
    return 'less than a minute more' if ms < @aMinute
    plural 'more minute', ms / @aMinute, true

  _areBanned: (err) ->
    typeof err is 'string' and err.indexOf('403') >= 0

  _handleBannedError: (comment, cb) ->
    debug "You are banned from /r/#{comment.subreddit}. Moving on..."
    cb()

  _handleUnknownError: (err, cb) ->
    debug "Unknown error submitting comment"
    debug err
    cb()

module.exports = Commenter
