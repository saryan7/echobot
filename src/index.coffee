rawjs   = require 'raw.js'
Echobot = require './echobot'
config  = require '../config'
debug   = require('debug') 'echobot'

module.exports = (namesThatEcho, options) ->
  # Initialize leddit API wrapper
  reddit = new rawjs config.username
  reddit.setupOAuth2 config.id, config.secret

  # Create edgy bot to carry out bidding of the alt right
  echobot = new Echobot namesThatEcho, reddit, options

  # Authenticate with leddit
  reddit.auth {
    username: config.username
    password: config.password
  }, (err, res) ->
    if err
      debug "Error authenticating Echobot with leddit"
      debug err
    else
      # Unleash the bot
      echobot.start()
