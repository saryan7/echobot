# Superclass for posts and comments
class Thing
  # Map values to thing for every key in data object
  constructor: (data) ->
    @[key] = val for key, val of data

module.exports = Thing
