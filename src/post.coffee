Thing       = require './thing'
{subreddit} = require '../config'

# A leddit post
class Post extends Thing
  # Fetch posts from leddit
  @fetch: (store, reddit, p, cb) ->
    reddit.hot {
      r:     subreddit
      after: store.nextAfter
      limit: 100
    }, cb

module.exports = Post
