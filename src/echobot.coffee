async     = require 'async'
q         = require 'q'
Store     = require './store'
Scanner   = require './scanner'
Commenter = require './commenter'
Timer     = require './timer'
config    = require '../config'
debug     = require('debug') 'echobot'

# Class for bot directive logic, requires array of echoey names and a reddit API
class Echobot
  constructor: (@namesThatEcho, @reddit, @options = {}) ->
    throw new Error "Names that echo are required" unless @namesThatEcho
    throw new Error "Leddit is required"           unless @reddit

    # Create stores for content from the faggiest community on the web
    @stores =
      post:    new Store 'post'
      comment: new Store 'comment'

    # Create timer to track request times
    @timer = new Timer()

    debug "No-comment mode enabled" if @options.nc

  # Execute order 1488
  start: ->
    debug "Echobot started! OY VEY!"
    async.forever (next) => @_poll next

  # Main loop
  _poll: (next) ->
    # 1. Fetch posts if post store is empty
    q.fcall( =>
      if @stores.post.empty()
        defer = q.defer()
        @stores.post.fetch @, null, defer
        defer.promise

    # 2. Fetch comments for top post if comment store is empty
    ).then( =>
      if @stores.comment.empty()
        defer = q.defer()
        @stores.comment.fetch @, @stores.post.dequeue(), defer
        defer.promise
    
    # 3. Scan comments for echoes and name the jews
    ).then( =>
      if echoeyComments = @_scan()
        defer = q.defer()
        @_comment echoeyComments, defer
        defer.promise

    # 4. Anudda Shoah!
    ).then -> next()

  # Scan comment store for echoes and return echoey comments
  _scan: ->
    scanner = new Scanner @stores.comment, @namesThatEcho
    scanner.scanStore()

  # Reply to echoey comments by naming the jews
  _comment: (echoeyComments, defer) ->
    return defer.resolve() if @options.nc # No-comment mode is on
    commenter = new Commenter echoeyComments, @
    commenter.nameTheJews defer

module.exports = Echobot
