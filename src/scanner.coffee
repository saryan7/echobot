indexOf = require 'lodash/array/indexOf'
union   = require 'lodash/array/union'
uniq    = require 'lodash/array/uniq'
limax   = require 'limax'
plural  = require 'pluralize'
debug   = require('debug') 'echobot'

# Searches a comment store for echoes
class Scanner
  constructor: (@commentStore, @namesThatEcho) ->

  # Returns array of echoey comments if found in comment store, false otherwise
  scanStore: ->
    return false if @commentStore.empty()

    commentsThatEcho = []

    subreddit = @commentStore.peek().subreddit
    debug "Scanning shitpost from /r/#{subreddit} for echoes..."

    while comment = @commentStore.dequeue()
      commentsThatEcho = union commentsThatEcho, @_scanComment(comment)

    echoCount = commentsThatEcho.length

    debug "#{plural 'echoey comments', echoCount, true} found"
    debug "OY VEY#{Array(echoCount + 1).join '!'}" if echoCount
    debug "THE GOYIM KNOW!" if echoCount >= 5
    debug "SHUT IT DOWN!"   if echoCount >= 10

    if echoCount then commentsThatEcho else false

  # Returns array echoey comments found in the top-level comment or its replies
  _scanComment: (comment) ->
    comment = comment.data if comment.data

    return [] unless comment.body

    commentsThatEcho = []

    # Add comment if it echoes
    if echoes = @_findEchoes comment.body
      commentsThatEcho.push
        parent:    comment.name
        subreddit: comment.subreddit
        text:      echoes

    # Recurse through any replies and add them to array if they echo
    if comment.replies
      for reply in comment.replies.data.children
        commentsThatEcho = union commentsThatEcho, @_scanComment(reply)

    commentsThatEcho

  # Returns string containing echoey names if any are found, false otherwise
  _findEchoes: (text) ->
    echoes = []

    # Strip specials from text
    text = limax text, ' '

    # Discard comment if it's our own
    return false if text.indexOf("brought to you by the jidf") >= 0

    # Search post for echoes
    for name in @namesThatEcho
      if text.indexOf(name.toLowerCase()) >= 0
        echoes.push name
        debug @_echo name

    if echoes.length then @_template(echoes) else false

  # Return comment body
  _template: (echoes) ->
    if echoes.length is 1
      numberOfHeebs = "is one Jew"
    else if echoes.length <= 10
      numberOfHeebs = "are #{@_numberToString echoes.length} Jews"
    else
      numberOfHeebs = "are #{plural 'Jew', echoes.length}"

    out = "There #{numberOfHeebs} mentioned in this post:\n\n"
    out += echoes.map(@_bullet).join("\n") + "\n\n"
    out += "This has been an automated message brought to you by the JIDF.\n\n"
    out += @_sub "This bot excludes Senator Bernie Sanders for obvious reasons."
    #debug "Message:\n#{out}"
    out

  # Return bulleted name
  _bullet: (name) ->
    "* #{name}"

  # Subscripts text for leddit format
  _sub: (text, n = 2) ->
    text.split(' ').map((w) -> "#{Array(n + 1).join '^'}#{w}").join(' ')

  # ((((GOLDBERGSHEKELSTEIN))))
  _echo: (name) ->
    "((((#{name.toUpperCase()}))))"

  # Return number as a string
  _numberToString: (n) ->
    switch n
      when 1  then 'one'
      when 2  then 'two'
      when 3  then 'three'
      when 4  then 'four'
      when 5  then 'five'
      when 6  then 'six'
      when 7  then 'seven'
      when 8  then 'eigth'
      when 9  then 'nine'
      when 10 then 'ten'

module.exports = Scanner
