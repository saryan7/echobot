config = require '../config'

# Keeps track of request times to avoid exceeding rate limit
class Timer
  contructor: (@lastRequestAt) ->

  # Sets @lastRequestAt to current time
  logRequest: ->
    @lastRequestAt = Date.now()

  # Returns ms until next request is allowed based on config.interval
  nextRequestIn: ->
    ms = config.interval * 1000 - (Date.now() - @lastRequestAt)
    ms = 0 if ms < 0
    ms

module.exports = Timer
