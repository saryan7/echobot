fs     = require 'fs'
plural = require 'pluralize'
debug  = require('debug') 'echobot'

# Queue for posts and comments, requires name of content type
class Store
  constructor: (@name) ->
    throw new Error("Store requires a name") unless @name
    
    @class     = require "./#{@name}.coffee" # Item class based on @name
    @storeFile = "#{__dirname}/../store/#{@name}.json"
    fileData   = @_read()
    @items     = fileData.items or []
    @nextAfter = fileData.nextAfter or null
    @offset    = 0

    debug "Store for #{plural @name} created"
    debug "#{plural @name, @size(), true} in store"

  # Adds item to back of queue
  enqueue: (item) ->
    @items.push item

  # Removes first item and returns it (think pop for stacks)
  # Not using @items.shift() because it's pleb-tier O(n). This is O(1).
  dequeue: ->
    return undefined if @empty()

    item = @items[@offset]

    if ++ @offset * 2 >= @items.length
      @items = @items.slice @offset
      @offset = 0

    @_write()

    item

  # Returns the first item in the queue without removing it
  peek: ->
    if @items.length > 0 then @items[@offset] else undefined

  # Returns number of items in queue
  size: ->
    @items.length - @offset

  # Returns true when queue is empty, false otherwise
  empty: ->
    @size() is 0

  # Fetches and stores items, calls fetch on @class with @_addItems() callback
  fetch: (bot, parent, defer) ->
    setTimeout ( =>
      debug "Jewing some #{plural @name}..."
      @class.fetch @, bot.reddit, parent, @_addItems =>
        bot.timer.logRequest()
        defer.resolve @
        #debug defer
        return
      return
    ), bot.timer.nextRequestIn()

  # Returns callback for @class.fetch() which creates and stores fetched items
  _addItems: (cb) ->
    (err, res) =>
      if err
        debug "Error fetching new #{plural @name}"
        debug err
        cb()
      else
        res = res.data if res.data
        @nextAfter = res.after
        for item in res.children
          @enqueue new @class(item.data)
        debug "#{plural @name, res.children.length, true} swindled"
        @_write cb
      return

  # Returns parsed store object from file or empty object if no file exists
  _read: ->
    try
      JSON.parse(fs.readFileSync(@storeFile, 'utf-8'))
    catch
      {}

  # Writes items to store file
  _write: (cb) ->
    try fs.unlinkSync @storeFile
    fs.writeFile @storeFile, JSON.stringify({
      items:     @items.slice @offset
      nextAfter: @nextAfter
    }, null, 2), 'utf-8', cb
    return

module.exports = Store
