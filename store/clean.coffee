fs     = require 'fs'
glob   = require 'glob'
plural = require 'pluralize'
debug  = require('debug') 'clean'

glob "#{process.cwd()}/store/*.json", (err, files) ->
  if err
    debug "Error cleaning store"
    debug err
  else
    fs.unlinkSync file for file in files
    debug "#{plural 'file', files.length, true} cleaned from store"
