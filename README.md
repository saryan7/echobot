Echobot
=======

It's annuda shoah.

### Setup

    sudo npm i -g coffee-script mocha && npm i

### Start the bot

    npm start

### Start the bot with commenting off

    npm run scan

### Run tests

    npm test

### Clean store files

    npm run clean
